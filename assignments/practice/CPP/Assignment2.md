This assignment tries to test your **Data Structures** and **C++ concepts**, if you are now able to understand it well. 

Below I have given you a list of cpp files, you can choose any one file and now you need to understand the data structures and c++ concepts(classes, virtual functions etc) used in that code and Comment the original code by writing data structure/C++ concepts used at each line. Who can find more things gets more points. Each one has to choose a separate file. [Please report your choice here](https://gitlab.iotiot.in/newbies/module0-c-cpp/issues/2#note_9945) accordingly other people can choose from remaining ones.

Please check [this](exp.cpp) example just to get an idea about to do it (its incomplete).

Please try to put your comments in original code so we can see the changes in code clearly also keep the comment in this format 

`// Assignment-comment ------write your comment here---------` to differenciate it with other comments.

1. [c1.cpp](programs/c1.cpp)
2. [c2.cpp](programs/c2.cpp)
3. [c3.cpp](programs/c3.cpp)
4. [c4.cpp](programs/c4.cpp)
5. [c5.cpp](programs/c5.cpp)
6. [t1.cpp](programs/t1.cpp)
7. [t2.cpp](programs/t2.cpp)
8. [t3.cpp](programs/t3.cpp)
9. [t4.cpp](programs/t4.cpp)
10. [t5.cpp](programs/t5.cpp)