# C language


- C language is developed by Dennis Ritchie in the year 1972.
- The main features of C language  is it has low-level access to memory,a simple set of keywords, and clean style.
- This is why C language is used for system programmings like an operating system or compiler development.

## Structure of a C program

C program basically consists of the following parts 

- Preprocessor Commands
- Functions
- Variables
- Statements & Expressions
- Comments
```bash
#include <stdio.h> 
int main()
{
/* my first 
program in C */
                                // this is ignored
printf("Hello, World! \n"); 

return 0;    
 }
```
- #include <stdio.h> is a preprocessor command, which tells a C compiler to include stdio.h file before going to actual compilation.
- int main() is the main function where the program execution begins.
- /*   */ will be ignored by the compiler because these are comment which help user to understand code.
    - /*   */ used for multiple line comments.
    - // single line comments.
- printf(...) is a function in C that displays/prints "Hello, World!" on the screen.
- return 0; terminates the main() function and returns the value 0.


### Compile & Execute C Program

- Open your text editor and write your first code(hello.c).
- Open terminal and start executing the code by following:
```bash
$ gcc hello.c(if no error it will take you to the next line or else fix the error)
$ ./a.out
```
**OUTPUT:**

 Hello, World!

## C Basic Syntax

- **Tokens:** A C program consists of various tokens(> a keyword,an identifier,a constant,a string literal,or a symbol).
- **Semicolons (;):** It tells that the statement is completed or terminated.
- **Comments:** Comments are ignored by the compiler.multiple line comments /*  */, single line comments //.
- **Identifiers:** It is a name used to identify a variable, function, or any other user-defined item.
    - it can start with any letter A to Z or a to z.
    - it can start with an underscore _ followed by zero or more letters, underscores.
    - it can start with digits (0 to 9).
    - it should not contain any punctuation characters such as @, $, and % . 
- C is a case sensitive programming language. Hence Iot and iot are two different identifiers.
- **Keywords:** keywords are reserved words that can't be used as constant or variable or any other identifier names.
    ![words](extras/c.png)
- **Whitespaces:** It is used in C to describe blanks, tabs, newline characters and comments. They are used to separate one part of a statement from another.

## C Data Types

- Data types are used for declaring variables or functions of different types.
- The type of a variable determines how much space it occupies and how the bit pattern stored is interpreted.

- The types in C can be classified as:
    - **Basic Types:** Arithmetic types are basic types,They are further classified into: (a) integer types and (b)    floating-point types.
    - **Enumerated types:** They are also arithmetic types which are used to define variables that can only be assigned certain discrete integer values.
    - **The type void:** This indicates that no value is available(null).
    - **Derived types:** They are (a) Pointer types, (b) Array types, (c) Structure types, (d) Union types and (e) Function types.

## C Variables

- Variables are the storage elements where we our data of different types.
- The data types we assign to the variable decides the memory size.
-  A variable name can be contents digit, letters and underscores charater.
- It must be begin with underscore or letter. Upper and lower case are distinct in C(case sensitive).

### Defining Variables in C

```bash
data_type variable_name;
```
data_type:har,int,float,double,etc
variable_name: any word like cake,chocolates etc

```bash
int i, j, k;
char c, ch;
float f, salary;
double d;
```
the commands tell compiler to create variables with above given names.

**Assigning values to variables**

```bash
data_type variable_name = value;
```
Some examples are:
```bash
int d = 3, f = 5; // definition and initializing d and f.
byte z = 22; // definition and initializes z.
char x = 'x'; // the variable x has the value 'x'.  
```
### Variable Declaration in C

- A variable declaration provides assurance to the compiler that there exists a variable with the given type and name.
- Variable declaration is helpful at the time of linking of the program.
- A variable declaration is useful when you are using multiple files and you define your variable in one of the files.
- extern keyword is used to declare a variable at any place.

**Example**
```bash
#include <stdio.h>
// Variable definition:
extern int a, b;
extern int c;
extern float f;
int main ()
{
// Variable definition:
int a, b;
int c;
float f;
// actual initialization
 a =5;
 b =15;
 c = a + b;
 printf("value of c : %d \n", c);
 f = 35.0/2.0;
 printf("value of f : %f \n", f);
 return 0;
}
```
**OUTPUT:**

value of c : 20 

value of f : 17.5

### Lvalues and Rvalues in C

There are two kinds of expressions in C:

- **lvalue**: lvalue is an expression that may appear as either the left-hand or right-hand side of an assignment.
- **rvalue**: rvalue is an expression may appear on the right- but not left-hand side of an assignment.
- Variables are lvalue appear on left side and numeric values are rvalues that appear only on right side.
- **Valid:**int g = 90;
- **Invalid:**50 = 30;

## C Constants

Constants are the values that the program may not alter during its execution. These are also called as literals.

### Defining Constants

There are two ways to define C constants:
- Using #define preprocessor.
- Using const keyword.

#### #define Preprocessor
```bash
#define identifier value
```
values are defined in identifier value place.

**Example**
```bash
#include <stdio.h>
#define LENGTH 7
#define WIDTH 7
#define NEWLINE '\n'
int main()
{
 int area;

 area = LENGTH * WIDTH;
 printf("value of area : %d", area);
 printf("%c", NEWLINE);
 return 0;
}
```
**OUTPUT:**

value of area: 49

#### const Keyword

```bash
const type variable = value;
```
we use const as prefix for this type.
```bash
#include <stdio.h>
int main()
{
 const int LENGTH = 7;
 const int WIDTH = 7;
 const char NEWLINE = '\n';
 int area;

 area = LENGTH * WIDTH;
 printf("value of area : %d", area);
 printf("%c", NEWLINE);
 return 0;
}
```
**OUTPUT:**

value of area: 49

## C Storage Classes

A storage class defines scope of variables or functions. 

storage classes types:
- auto
- register
- static
- extern

### The auto Storage Class

An auto storage class is default storage class used for all local variables.
```bash
{
	int embedded;
	auto int c;(both c & embedded are local variables)
}
```
### The register Storage Class

The register storage class is used to define local variable which is stored in register instead of RAM.
```bash
{
 register int counter;
}
```

### The static Storage Class

- The static storage class is used to keep the local variable in existence during the execution the program as many times it is.
- The static modifier can also be applied to global variables which makes variable's scope to be restricted within the file.
```bash
#include <stdio.h>
/* function declaration */
void func(void);
static int count = 5; /* global variable */
main()
{
 while(count!=-1)
 {
 func();
 }
 return 0;
}
/* function definition */
void func( void )
{
 static int i = 5; /* local static variable */
 i++;
 printf("i is %d and count is %d\n", i, count);
 count--;
} 
```
**OUTPUT:**

i is 6 and count is 5

i is 7 and count is 4

i is 8 and count is 3

i is 9 and count is 2

i is 10 and count is 1

i is 11 and count is 0

### The extern Storage Class

- The extern is a storage class that is used for accessing this global variable or function in another file.
- Variables defined by extern are available throughout the progam files.
- The extern modifier is most commonly used when there are two or more files havingthe same global variables.

#### Example

**main.c**
```bash
#include <stdio.h>
int count ;
extern void write_extern();
main()
{
 write_extern();
}
```
**count.c**
```bash
#include <stdio.h>
extern int count;
void write_extern(void)
{
 count = 5;
 printf("count is %d\n", count);
}
```
**OUTPUT:**

count is 5

**Commands to run **
```bash
$ gcc -o main main.c count.c
$ ./main
```

## Decision Making in C

- It is required to take step after certain conditions.
- If the given condition is true is perform one set instruction or else does another set.
- In C it is considered zero or non-null value as true and non-zero or null value as false. 

### types of decision making statements.

#### if statement

Here if the condition is true then it executes the statements written in the if statement block and if statement is false then it will execute first statement written after if statement block.

**Syntax**
```bash
if(boolean_expression)
{
 /* statement(s) will execute if the boolean expression/condition is true */
}
```
**Example**
```bash
#include <stdio.h>
int main ()
{
 /* local variable definition */
 int a = 5;
 /* check the boolean condition using if statement */
 if( a==5)
 {
 /* if condition is true then print the following */
 printf("a is 5\n" );
 }
 return 0;
}
```
**OUTPUT:**

a is 5

### if...else statement
- Here we have else block statements to perform when the given condition is false.
**Syntax**
```bash
if(boolean_expression)
{
 /* statement(s) will execute if the boolean expression is true */
}
else
{
 /* statement(s) will execute if the boolean expression is false */
}
```
**Example**
```bash
#include <stdio.h>
int main ()
{
 int a = 100;
 if( a < 20 )
 {
 printf("a is less than 20\n" );
 }
 else
 {
 /* if condition is false then print the following */
 printf("a is not less than 20\n" );
 }
 return 0;
}
```
**OUTPUT:**

a is not less than 20

### The if...else if...else Statement

- Here we check more than one condition possibility.
When using if , else if , else statements there are few points to keep in mind:
- An if can have zero or one else's and it must come after any else if's.
- An if can have zero to many else if's and they must come before the else.
- Once an else if succeeds, none of the remaining else if's or else's will be tested.

**Syntax**
```bash
if(boolean_expression 1)
{
 /* Executes when the boolean expression 1 is true */
}
else if( boolean_expression 2)
{
 /* Executes when the boolean expression 2 is true */
}
else if( boolean_expression 3)
{
 /* Executes when the boolean expression 3 is true */
}
else
{
 /* executes when the none of the above condition is true */
}
```
**Example**
```bash
#include <stdio.h>
int main ()
{
 /* local variable definition */
 int a = 10;
 if( a == 10 )
 {
 printf("Value of a is 10\n" );
 }
 else if( a == 20 )
 {
 printf("Value of a is 20\n" );
 }
 else if( a == 30 )
 {}
 printf("Value of a is 30\n" );
 }
 else
 {
 printf("None of the values are matching\n" );
 }
 return 0;
}
```
**OUTPUT:**

Value of a is 10

### Nested if statements

In nested if statements we can use one if or else if statement inside another.

**Syntax**
```bash
if( boolean_expression 1)
{
 /* Executes when the boolean expression 1 is true */
 if(boolean_expression 2)
 {
 /* Executes when the boolean expression 2 is true */
 }
} 
```
**Example**
```bash
#include <stdio.h>
int main ()
{
 int a = 100;
 int b = 200;
 if( a == 100 )
 {
 if( b == 200 )
 {
 printf("Value of a is 100 and b is 200\n" );
 }
 }
 printf("Exact value of a is : %d\n", a );
 printf("Exact value of b is : %d\n", b );
 return 0;
}
```
**OUTPUT:**

Value of a is 100 and b is 200

Exact value of a is : 100

Exact value of b is : 200

### Switch Statement

The switch statement allows us to execute one code block among many alternatives.

**Syntax**

```bash
switch(expression){
 case constant-expression :
 statement(s);
 break; /* optional */
 case constant-expression :
 statement(s);
 break; /* optional */

 /* you can have any number of case statements */
 default : /* Optional */
 statement(s);
}
```
**Example**

```bash
#include <stdio.h>
int main ()
{
 /* local variable definition */
 char grade = 'B';
 switch(grade)
 {
 case 'A' :
 printf("Excellent!\n" );
 break;
 case 'B' :
 case 'C' :
 printf("Well done\n" );
 break;
 case 'D' :
 printf("You passed\n" );
 break;
 case 'F' :
 printf("Better try again\n" );
 break;
 default :
 printf("Invalid grade\n" );
 }
 printf("Your grade is %c\n", grade );
 return 0;
}
```
**OUTPUT:**

Excellent!

Your grade is A

### Nested switch statements

We have one switch part in other switch.

**Syntax**
```bash
switch(ch1) {
 case 'A':
 printf("This A is part of outer switch" );
 switch(ch2) {
 case 'A':
 printf("This A is part of inner switch" );
 break;
 case 'B': /* case code */
 }
 break;
 case 'B': /* case code */
}
```
**Example**
```bash
#include <stdio.h>
int main ()
{
/* local variable definition */
 int a = 100;
 int b = 200;
 switch(a) {
 case 100:
 printf("This is part of outer switch\n", a );
 switch(b) {
 case 200:
 printf("This is part of inner switch\n", a );
 }
 }
 printf("Exact value of a is : %d\n", a );
 printf("Exact value of b is : %d\n", b );
 return 0;
}
```
**OUTPUT:**

This is part of outer switch

This is part of inner switch

Exact value of a is : 100

Exact value of b is : 200

## C Loops

Here we execute a set of code block until the given condition becomes false.

### while loop in C

A while loop executes code block/statements until the given condition is true.

**Syntax**

```bash
while(condition)
{
statement(s);
}
```
**Flow Diagram**

![while](extras/flow1.png)

**Example**
```bash
#include <stdio.h>
int main ()
{
	int mt = 0;
	while(mt!=10)
	{
		printf("Value of mt is:%d\n",mt);
		mt++;
	}
}
```
**OUTPUT:**

Value of mt is:0

Value of mt is:1

Value of mt is:2

Value of mt is:3

Value of mt is:4

Value of mt is:5

Value of mt is:6

Value of mt is:7

Value of mt is:8

Value of mt is:9

### for loop in C

For loop is also used to iterate the statement.but the control structure is different from while.

**Syntax**
```bash
for (init;condition;increment)
{
	statement(s);
}
```
**Flow Diagram**

![forloop](extras/flow2.png)

**Example**

```bash
#include <stdio.h>
int main ()
{
	int mt=0,i;
	for(i=0;i<10;i++)
	{
		printf("Value of mt is:%d\n",mt);
		mt++;
	}
}
```
**OUTPUT:**

Value of mt is:0

Value of mt is:1

Value of mt is:2

Value of mt is:3

Value of mt is:4

Value of mt is:5

Value of mt is:6

Value of mt is:7

Value of mt is:8

Value of mt is:9

### do...while loop in C

Here the code block is executed at least once because it executes the loop first and after checks the condition.

**Syntax**

```bash
do
{
statement(s);
}while( condition );
```

**Flow Diagram**

![do](extras/flow3.png)

**Example**

```bash
#include <stdio.h>
int main ()
{
	int mt=0;
	do
	{
		printf("Value of mt is:%d\n",mt);
		mt++;
	}while(mt!=10);
	return 0;
}
```

**OUTPUT:**

Value of mt is:0

Value of mt is:1

Value of mt is:2

Value of mt is:3

Value of mt is:4

Value of mt is:5

Value of mt is:6

Value of mt is:7

Value of mt is:8

Value of mt is:9


### The Infinite Loop

A loop becomes an infinite loop if a condition never becomes false. for loop is used for this purpose.when all the three expressions are none it will be infinite loop.

**Syntax**

```bash
#include <stdio.h>
int main ()
{
for( ; ; )
{
printf("This loop will run forever.\n");
}
return 0 ;
}
```
## C Functions

Function is a group of statements that performs a task.main() is a function that is present in every c progam.
A function declaration tells the compiler about a function's name, return type, and parameters.

### Defining a Function

```bash
return_type function_name( parameter list )
{
body of the function
}
```

A function definition in C programming language consists of a function header and a function body. Here are all the parts of a function:


Return Type: A function may return a value. The return_type is the data type of the value the function returns.


Function Name: This is the actual name of the function.


Parameters: A parameter is like a placeholder. When a function is invoked, you pass a value to the parameter. which is called as actual parameter or argument.


Function Body: The function body contains a collection of statements that define what the function does.

**Example**

```bash
/* function returning the max between two numbers */
int max(int num1, int num2)
{
/* local variable declaration */
int result;
if (num1 > num2)
result = num1;
else
result = num2;
return result;
}
```
### Function Declarations

A function declaration tells the compiler about a function name and how to call the function. 

A function declaration has the following parts:

**return_type function_name( parameter list );**

**int max(int num1, int num2);**-function declaration

Parameter names are not important in function declaration only their type is required.
**int max(int, int);**

### Calling a Function

To call a function,we need to pass the required parameters along with function name.

**Example**
```bash
#include <stdio.h>
/* function declaration */
int max(int num1, int num2);
int main ()
{
/* local variable definition */
int a = 100 ;
int b = 200 ;
int ret;
/* calling a function to get max value */
ret = max(a, b);
printf( "Max value is : %d\n", ret );
return 0 ;
}
/* function returning the max between two numbers */
int max(int num1, int num2)
{
/* local variable declaration */
int result;
if (num1 > num2)
result = num1;
else
result = num2;
return result;
}
```
**OUTPUT:**

Max value is : 200


### Function Arguments

While calling functions we need to pass parameters, they can be passed in two ways:

1.Function call by value

2.Function call by reference

3.Function parameters which is called formal parameters.


#### Function call by value

Here the function copies the actual value of an argument into the formal parameter of the function. if any changes made to the parameter inside the function have no effect on the arguments.

**Syntax**
```bash
/* function definition to swap the values */
void swap(int x, int y)
{
int temp;
temp = x; /* save the value of x */
x = y;
/* put y into x */
y = temp; /* put x into y */
return;
}
```

**Example**
```bash
#include <stdio.h>
/* function declaration */
void swap(int x, int y);
int main ()
{
/* local variable definition */
int a = 100;
int b = 200;
printf("Before swap, value of a : %d\n", a );
printf("Before swap, value of b : %d\n", b );
/* calling a function to swap the values */
swap(a, b);
printf("After swap, value of a : %d\n", a );
printf("After swap, value of b : %d\n", b );
return 0;
}
void swap(int x, int y)
{
int temp;
temp = x; /* save the value of x */
x = y;
/* put y into x */
y = temp; /* put x into y */
return;
}
```
**OUTPUT:**

Before swap, value of a : 100

Before swap, value of b : 200

After swap, value of a : 100

After swap, value of b : 200

## C Scope Rules

A scope in is a region where the defined variable have its existence and beyond that variable cannot be accessed.

- Inside a function or a block which is called local variables,
- Outside of all functions which is called global variables.

### Local Variables

These are the Variables that are declared inside a function or block.They can be used only inside that function or block of code.

**Example**
```bash
#include <stdio.h>
int main ()
{
/* local variable declaration */
int a, b;
int c;
/* actual initialization */
a = 10 ;
b = 20 ;
c = a + b;
printf ("value of a = %d, b = %d and c = %d\n", a, b, c);
return 0 ;
}
```
### Global Variables

Global variables are defined outside of a function, usually on top of the program.Global variable is available for entire program after its declaration. 

**Example**

```bash
#include <stdio.h>
/* global variable declaration */
int g;
int main ()
{
/* local variable declaration */
int a, b;
/* actual initialization */
a = 10 ;
b = 20 ;
g = a + b;
printf ("value of a = %d, b = %d and g = %d\n", a, b, g);
return 0 ;
}

A program can have same name for local and global variables but value of local variable inside a function will take preference. Following is an example:

#include <stdio.h>
/* global variable declaration */
int g = 20 ;
int main ()
{
/* local variable declaration */
int g = 10 ;
printf ("value of g = %d\n",
g);
return 0 ;
}
```
**OUTPUT:**

1.value of a = 10, b = 20 and g =30
2.value of g = 10

### Formal Parameters

Function parameters called as formal parameters, are treated as local variables within that function and they will take preference over the global variables.

**Example**

```bash
#include <stdio.h>
/* global variable declaration */
int a = 20 ;
int main ()
{
/* local variable declaration in main function */
int a = 10 ;
int b = 20 ;
int c = 0 ;
printf ("value of a in main() = %d\n",
c = sum( a, b);
printf ("value of c in main() = %d\n",
a);
c);
return 0 ;
}
/* function to add
int sum(int a, int
{
printf ("value
printf ("value
two integers */
b)
of a in sum() = %d\n",
of b in sum() = %d\n",
a);
b);
return a + b;
}
```

**OUTPUT:**

value of a in main() = 10

value of a in sum() = 10

value of b in sum() = 20

value of c in main() = 30

### Initializing Local and Global Variables

local variable is not initialized by the system, we must initialize it where as Global variables are initialized automatically by the system.

```bash
Data Type      Initial Default Value
int                    0
char                  '\0'
float                  0
double                 0
pointer                NULL
```

## C Arrays

- C programming provides a certain type of data structure known as arrays, which stores elements with same data type. 
- We  we can declare one variable with an array such as num[99] instead of num[0], num[1], num[2]...num[99]. 
- Every element is accessed by specific index.
- All arrays consist of sequential memory locations. 
	- The lower address[0] - first element.
	- The highest address[99] - for the last element.> considering above case.

### Declaring Arrays

```bash
type arrayName [ arraySize ];
```
type - data type (e.g. int, double, char, float). 
arratName - variable name.
[arraySize]- we can define n number of size for an array. This is a single dimensional array.

**Example**

```bash
double balance[10];
```
### Initializing Arrays

We can initialize array in C either one by one or using a single statement.
```bash
double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
```
The elements in {} brackets must not be lager than we declared in [] brackets.
```bash
double balance[] = {1000.0, 2.0, 3.4, 17.0, 50.0};
```
you can omit size which means the size bigger.

### Accessing Array Elements

Array elements are accessed uisng their index values.

**Example**
```bash
#include <stdio.h>
int main ()
{
	int n[ 10 ]; /* n is an array of 10 integers */
	int i,j;
	/* initialize elements of array n to 0 */
	for ( i = 0; i < 10; i++ )
	{
		n[ i ] = i + 100; /* set element at location i to i + 100 */
	}
	/* output each array element's value */
	for (j = 0; j < 10; j++ )
	{
		printf( "Element[%d] = %d\n" , j, n[j] );
	}
	return 0;
}
```
**OUTPUT:**
Element[0] = 100

Element[1] = 101

Element[2] = 102

Element[3] = 103

Element[4] = 104

Element[5] = 105

Element[6] = 106

Element[7] = 107

Element[8] = 108

Element[9] = 109

### Multi-dimensional Arrays

C programming language allows multidimensional arrays.
```bash
type name[size1][size2]...[sizeN];
```
```bash
int threedim[5][10][4];
```
it creates three dimensional 5 . 10 . 4 integer array:

#### Two-Dimensional Arrays

Simple Multi-dimensional Arrays is Two-Dimensional Arrays.

```bash
type arrayName [ x ][ y ];
```

- A two-dimensional array will have x number of rows and y number of columns.
- every element in array a is identified by an element name of the form a[ i ][ j ]. i and j are the subscripts.

#### Initializing Two-Dimensional Arrays

```bash
int a[3][4] = {
{0, 1, 2, 3} ,   /*initializers for row indexed by 0 */
{4, 5, 6, 7} ,   /*initializers for row indexed by 1 */
{8, 9, 10, 11}   /*initializers for row indexed by 2 */
};
```
#### Accessing Two-Dimensional Array Elements

An element in 2-dimensional array is accessed by using the subscripts[i][j].
```bash
int val = a[2][3];
```
**Example**

```bash
#include <stdio.h>
int main ()
{
	/* an array with 5 rows and 2 columns*/
	int a[5][2] = { {0,0}, {1,2}, {2,4}, {3,6},{4,8}};
	int i, j;
	/* output each array element's value */
	for ( i = 0; i < 5; i++ )
		{
		for ( j = 0; j < 2; j++ )
			{
				printf( "a[%d][%d] = %d\n" , i,j, a[i][j] );
			}
		}
	return 0;
}
```
**OUTPUT:**
```bash
a[0][0] = 0
a[0][1] = 0
a[1][0] = 1
a[1][1] = 2
a[2][0] = 2
a[2][1] = 4
a[3][0] = 3
a[3][1] = 6
a[4][0] = 4
a[4][1] = 8
```
### Passing Arrays as Function Arguments

If we want to pass single-dimension array as an argument in a function, then we have to declare function formal parameter:

**Way1:**

Formal parameters as a pointer.
```bash
void myFunction(int *param)
{
.
.
.
}
```
**Way2:**

Formal parameters as a sized array.
```bash
void myFunction(int param[10])
{
.
.
.
}
```
**Way3:**

Formal parameters as a Unsized array.****
```bash
void myFunction(int param[])
{
.
.
.
}
```
**Example**

an array as an argument along with another argument.

```bash
double getAverage(int arr[], int size)
{
	int i;
	double avg;
	double sum;
	for (i = 0; i < size; ++i)
		{
			sum += arr[i];
		}
	avg = sum / size;
	return avg;
}
```
calling the function.
```bash
#include <stdio.h>
/* function declaration */
double getAverage(int arr[], int size);
int main ()
{
	/* an int array with 5 elements */
	int balance[5] = {1000, 2, 3, 17, 50};
	double avg;
	/* pass pointer to the array as an argument */
	avg = getAverage( balance, 5 ) ;
	/* output the returned value */
	printf( "Average value is: %f ", avg );
	return 0;
}
```
**OUTPUT:**

Average value is: 214.400000

### Return array from function

- C programming language does not allow to return an entire array as an argument to a function.
- We can only return a pointer to an array by specifying the array's name without an index. 
```bash
int * myFunction()
{
.
.
.
}
```
C does not return the address of a local variable to the outside of the function so we need to define the local variable as static variable.


> generate 5 random numbers and return them using an array
```bash
include <stdio.h>
/* function to generate and return random numbers */
int * getRandom( )
{
	static int r[10];
	int i;
	/* set the seed */
	srand( (unsigned)time( NULL ) );
	for ( i = 0; i < 5; ++i)
		{
			r[i] = rand();
			printf( "r[%d] = %d\n", i, r[i]);
		}
	return r;
}
/* main function to call above defined function */
int main ()
{
	/* a pointer to an int */
	int *p;
	int i;
	p = getRandom();
	for ( i = 0; i < 5; i++ )
		{
			printf( "*(p + %d) : %d\n", i, *(p + i));
		}
	return 0;
}
```
**OUTPUT:**
```bash
r[0] = 1559995669
r[1] = 2089309528
r[2] = 938233732
r[3] = 1312406073
r[4] = 1411482204
*(p + 0) : 1559995669
*(p + 1) : 2089309528
*(p + 2) : 938233732
*(p + 3) : 1312406073
*(p + 4) : 1411482204
```
### Pointer to an Array

An array name is a constant pointer 
```bash
double balance[50];
```
*(balance + 4) is a legitimate way of accessing the data at balance[4].

```bash
double *p;
double balance[10];
p = balance;
```
Once we store the address of the first element in p, we can access array elements using *p, *(p+1), *(p+2) and so on.
```bash
#include <stdio.h>
int main ()
{
	/* an array with 5 elements */
	double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
	double *p;
	int i;
	p = balance;
	/* output each array element's value */
	printf( "Array values using pointer\n");
	for ( i = 0; i < 5; i++ )
		{
			printf("*(p + %d) : %f\n", i, *(p + i) );
		}
	printf( "Array values using balance as address\n");
	for ( i = 0; i < 5; i++ )
		{
			printf("*(balance + %d) : %f\n", i, *(balance + i) );
		}
	return 0;
}
```
**OUTPUT:**

```bash
Array values using pointer
*(p + 0) : 1000.000000
*(p + 1) : 2.000000
*(p + 2) : 3.400000
*(p + 3) : 17.000000
*(p + 4) : 50.000000
Array values using balance as address
*(balance + 0) : 1000.000000
*(balance + 1) : 2.000000
*(balance + 2) : 3.400000
*(balance + 3) : 17.000000
*(balance + 4) : 50.000000
```
## C Pointers

Pointers is use to store an address of a variable. Every variable has a specific address which we can access using operator ampersand(&).

**Finding address of a Variable:**

```bash
#include <stdio.h>
int main ()
{
	int a;
	printf("Address of a is:%x",&a);
}
```
**Output:**

Address of a is:58f38014 

###  Pointers Declaration

```bash
type *var-name;
```
- type - data type.
- var-name - pointer variable. 
- The asterisk(*) is being used to designate a variable as a pointer.

**Valid pointer declaration:**
```bash
int *ip;    /*pointer to an integer */
double *dp; /*pointer to a double */
float *fp;  /*pointer to a float */
char *ch    /*pointer to a character */
```

## C Strings

- The string is one dimensional array of characters which is terminates by null character "\0".

**char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};**

- String consisting of the word "Hello". 
- To hold the null character at the end of the array, the size of the character array should be one more than the number of characters in the word "Hello".

**Using array initialization:**

char greeting[] = "Hello";

- We need not to place the null character at the end of a string constant. The C compiler automatically places the '\0'.

**Example**

```bash
#include <stdio.h>
int main ()
{
char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
printf("Greeting message: %s\n", greeting );
return 0;
}
```
**OUTPUT:**

Greeting message: Hello

**String Functions:**

- strcpy(s1, s2); Copies string s2 into string s1.
- strcat(s1, s2); Concatenates string s2 onto the end of string s1.
- strlen(s1); Returns the length of string s1.
- strcmp(s1, s2); Returns 0 if s1 and s2 are the same; less than 0 if s1<s2; greater than 0 if s1>s2.
- strchr(s1, ch); Returns a pointer to the first occurrence of character ch in string s1.
- strstr(s1, s2); Returns a pointer to the first occurrence of string s2 in string s1.

**Example**

```bash
#include <stdio.h>
#include <string.h>
int main ()
{
char str1[12] = "Hello";
char str2[12] = "World";
char str3[12];
int len ;
/* copy str1 into str3 */
strcpy(str3, str1);
printf("strcpy( str3, str1) :
%s\n", str3 );
/* concatenates str1 and str2 */
strcat( str1, str2);
printf("strcat( str1, str2):
%s\n", str1 );
/* total lenghth of str1 after concatenation */
len = strlen(str1);
printf("strlen(str1) : %d\n", len );
return 0;
}
```

**OUTPUT:**
```bash
strcpy( str3, str1) : Hello
strcat( str1, str2): HelloWorld
strlen(str1) : 10
```
## C Structures

- C arrays allows us to declare several data items which is hold by a type of variable. 
- C structures is a user defined data type which can hold different types of data types.
- Structures are useful to record the data(different details).

### Defining a Structure

- We use the struct statement to define structure. 
- The struct statement defines a new data type, with more than one member for your program.

**Syntax**
```bash
struct [structure tag] (The structure tag is optional)
{
member definition;
member definition;
...
member definition;
} [one or more structure variables];
```

**Example**

```bash
struct Student
{
	char name[50];
	char branch[50];
	int year;
	int clg_id;
} student;
```

### Accessing Structure Members

- To access the members of structure we use dot(.) operator. 
- The dot is placed between the structure variable name and the structure member that we wish to access. 
- We use struct keyword to define variables of structure type.

**Example**

```bash
#include <stdio.h>
#include <string.h>
struct Student
{
	char name[50];
	char branch[50];
	char year[50];
	int clg_id;
};
int main( )
{
	struct Student student1;
	struct Student student2;
	/* Declare student1 of type Student */
	/* Declare student2 of type Student */
	/* Student 1 specification */
	strcpy( student1.name, "ABC" );
	strcpy( student1.branch, "EnTC" );
	strcpy( student1.year, "Final Year" );
	student1.clg_id = 1619;
	/* Student 2 specification */
	strcpy( student2.name, "XYZ" );
	strcpy( student2.branch, "Comp" );
	strcpy( student2.year, "Third Year" );
	student2.clg_id = 1630;
	/* print student1 info */
	printf( "Student 1 Name : %s\n" , student1.name);
	printf( "Student 1 Branch : %s\n" , student1.branch);
	printf( "Student 1 Year : %s\n" , student1.year);
	printf( "Student 1 College id : %d\n" , student1.clg_id);

	/* print student1 info */
	printf( "Student 2 Name : %s\n" , student2.name);
	printf( "Student 2 Branch : %s\n" , student2.branch);
	printf( "Student 2 Year : %s\n" , student2.year);
	printf( "Student 2 College id : %d\n" , student2.clg_id);
	return 0;
}
```
**OUTPUT:**

```bash
Student 1 Name : ABC
Student 1 Branch : EnTC
Student 1 Year : Final Year
Student 1 College id : 1619
Student 2 Name : XYZ
Student 2 Branch : Comp
Student 2 Year : Third Year
Student 2 College id : 1630
```

### Structures as Function Arguments

We can also pass structure as function argument.

**Example**

```bash
#include <stdio.h>
#include <string.h>
struct Student
{
	char name[50];
	char branch[50];
	char year[50];
	int clg_id;
};
void studentprint(struct Student student);
void studentprint(struct Student student)
{
	printf( "Student Name : %s\n" , student.name);
	printf( "Student Branch : %s\n" , student.branch);
	printf( "Student Year : %s\n" , student.year);
	printf( "Student College id : %d\n" , student.clg_id);
}
int main( )
{
	struct Student student1;
	struct Student student2;
	/* Declare student1 of type Student */
	/* Declare student2 of type Student */
	/* Student 1 specification */
	strcpy( student1.name, "ABC" );
	strcpy( student1.branch, "EnTC" );
	strcpy( student1.year, "Final Year" );
	student1.clg_id = 1619;
	/* Student 2 specification */
	strcpy( student2.name, "XYZ" );
	strcpy( student2.branch, "Comp" );
	strcpy( student2.year, "Third Year" );
	student2.clg_id = 1630;
	/*Print the student information*/
	studentprint(student1);
	studentprint(student2);
	return 0;
}
```

**OUTPUT:**

```bash
Student Name : ABC
Student Branch : EnTC
Student Year : Final Year
Student College id : 1619
Student Name : XYZ
Student Branch : Comp
Student Year : Third Year
Student College id : 1630
```

### Pointers to Structures

defining pointers to structures. 

```bash
struct Students *struct_pointer;
```
storing the address of structure variable to pointer.

```bash
struct_pointer = &student1;
```
To access the members of a structure using a pointer.

```bash
struct_pointer->name;
```

**Example**

```bash
#include <stdio.h>
#include <string.h>
struct Student
{
	char name[50];
	char branch[50];
	char year[50];
	int clg_id;
};
void studentprint(struct Student *student);
void studentprint(struct Student *student)
{
	printf( "Student Name : %s\n" , student->name);
	printf( "Student Branch : %s\n" , student->branch);
	printf( "Student Year : %s\n" , student->year);
	printf( "Student College id : %d\n" , student->clg_id);
}
int main( )
{
	struct Student student1;
	struct Student student2;
	/* Declare student1 of type Student */
	/* Declare student2 of type Student */
	/* Student 1 specification */
	strcpy( student1.name, "ABC" );
	strcpy( student1.branch, "EnTC" );
	strcpy( student1.year, "Final Year" );
	student1.clg_id = 1619;
	/* Student 2 specification */
	strcpy( student2.name, "XYZ" );
	strcpy( student2.branch, "Comp" );
	strcpy( student2.year, "Third Year" );
	student2.clg_id = 1630;
	/*Print the student information*/
	studentprint(&student1);
	studentprint(&student2);
	return 0;
}
```

**OUTPUT:**

```bash
Student Name : ABC
Student Branch : EnTC
Student Year : Final Year
Student College id : 1619
Student Name : XYZ
Student Branch : Comp
Student Year : Third Year
Student College id : 1630
```

## Typedef

Through typedef keyword, we can create a new type name.

```bash
typedef unsigned char byte;
```
Now we can use BYTE as an abbreviation for the type unsigned char.

```bash
byte b1,b2;
```

**Example**

typedef for structure

```bash
#include <stdio.h>
#include <string.h>
typedef struct Students
{
	char name[50];
	char branch[50];
	char year[50];
	int clg_id;
}Student;
int main( )
{
	Student student;
	/* Declare student1 of type Student */
	/* Declare student2 of type Student */
	/* Student 1 specification */
	strcpy( student.name, "ABC" );
	strcpy( student.branch, "EnTC" );
	strcpy( student.year, "Final Year" );
	student.clg_id = 1619;
	/* print student info */
	printf( "Student Name : %s\n" , student.name);
	printf( "Student Branch : %s\n" , student.branch);
	printf( "Student Year : %s\n" , student.year);
	printf( "Student College id : %d\n" , student.clg_id);
	return 0;
}
```

**OUTPUT:**

```bash
Student Name : ABC
Student Branch : EnTC
Student Year : Final Year
Student College id : 1619
```

### typedef vs #define

- The typedef has the advantage that it obeys the scope rules. 
- That means you can use the same name for the different types in different scope. 
- It can have file scope or block scope in which declare. In other words, #define does not follow the scope rule.

```bash
#include <stdio.h>
#define TRUE 1
#define FALSE 0
int main( )
{
	printf( "Value of TRUE : %d\n", TRUE);
	printf( "Value of FALSE : %d\n", FALSE);
	return 0 ;
}
```

**OUTPUT:**

```bash
Value of TRUE : 1
Value of FALSE : 0
```



















































 





