# CPP Programming Basics


## Part 1: Compulsory for AI and Optional for IoT Preparatory

### Cpp Programming Basics

**Hello World**

```bash
#include <iostream>

int main()
{
std::cout << "Hello World!" << std::endl;
}

```
- #include <iostream> is a preprocessor directive that includes the content of the standard C++ header file
iostream.
- iostream is a standard library that contains standard input and output files.
- The std (I/O) streams is used get input from user and output is shown to user(terminal).
- int main() { ... } the one function that is used only once in every program.it will always return a number of the int type.
- when we write returns ^0 or EXIT_SUCCESS we mean program is successfully executed.
- If no return statement is present, the main function returns^0 by default. it is not compulsory to write return 0.
- Void type doesn't return any value.
```bash
std::cout << "Hello World!" << std::endl; prints "Hello World!" as output
```
- std is a namespace, and :: is the scope resolution operator that looks for name within a namespace.
- We use :: to show we want to use cout from the std namespace.
- std::cout is the standard output stream object in iostream that prints to the standard output (stdout).
- << this is stream insertion operator, it inserts an object into the stream object.
- It also allows stream insertions to be chained: std::cout << "Foo" << " Bar"; prints "FooBar" to the console.
- The stream insertion operator for character string literals is defined in file iostream.
- std::endl is a special I/O stream manipulator object,defined in file iostream. Inserting it into a stream changes the state of the stream.
- std::endl does two things: 
        - it inserts the end-of-line character.
        - it flushes the stream buffer to force the text to show up on the console. 
```bash
std::cout << "Hello World! \n ";( \n moves to newline)
```
- this is another way to show output on console.
- semicolon(;) tells compiler that statement is ended.

### Comments

- comments are the text we keep inside program to explain the code to users.
- comments are not interpreted by C++ compiler.
There are two types of comments in C++:

**Single-Line Comments**
The double forward-slash sequence // will mark all text until a newline as a comment.

```bash
int main()
{
// This is a single-line comment.
int a; // this also is a single-line comment
int i; // this is another single-line comment
}
```
**C-Style/Block Comments**


- The /* is used to declare the start of the comment block and */is used to declare the end of comment. 
- All text between the start and end sequences is interpreted as a comment,whatever the text is(even valid c-syntax).
- These are sometimes called "C-style" comments, as this comment syntax is from C++'s predecessor language, C.

```bash
int main()
{
/*
* This is a block comment.
*/
int a;
}
```
```bash
void SomeFunction( /* argument 1 */ int a, /* argument 2 */ int b);
```
#### Importance of Comments

- Explicit documentation of code to make it easier to read/maintain
- Explanation of the purpose and functionality of code
- Details on the history or reasoning behind the code
- Placement of copyright/licenses, project notes, special thanks, contributor credits, etc. directly in the source
code.

###  The standard C++ compilation process

- A compiler converts programming language into another which is directly executed by a computer. this process is called compilation.
- C++ compilation process is taken from its "parent" language, C.

steps of compilation in C++:
```bash

- The C++ preprocessor copies the contents of any included header files into the source code file, generates
macro code, and replaces symbolic constants defined using #define with their values.
The expanded source code file produced by the C++ preprocessor is compiled into assembly language
appropriate for the platform.
The assembler code generated by the compiler is assembled into appropriate object code for the platform.
The object code file generated by the assembler is linked together with the object code files for any library
functions used to produce an executable file.
```

### Function

A function is a unit of code that represents a sequence of statements.
Functions can accept arguments or values and return a single value (or not). To use a function, a function call is
used on argument values and the use of the function call itself is replaced with its return value.
Every function has a type signature -- the types of its arguments and the type of its return type.
Functions are inspired by the concepts of the procedure and the mathematical function.














